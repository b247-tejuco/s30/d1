// create docs to use for this discussion

db.fruits.insertMany([
		{
			name: "Apple",
			color: "Red",
			stock: 20,
			price: 40,
			supplier_id: 1,
			onSale: true,
			origin: ["Philippines", "US"]
		},

		{
			name: "Banana",
			color: "Yellow",
			stock: 15,
			price: 20,
			supplier_id: 2,
			onSale: true,
			origin: ["Philippines", "Ecuador"]
		},
		{
			name: "Kiwi",
			color: "Green",
			stock: 25,
			price: 50,
			supplier_id: 1,
			onSale: true,
			origin: ["US", "China"]
		},
		{
			name: "Mango",
			color: "Yellow",
			stock: 10,
			price: 120,
			supplier_id: 2,
			onSale: false,
			origin: ["Philippines", "India"]
		}
	])

	// [SECTION] MongoDB Aggregation
	// Used to generate manipulated data and perform operations to create filtered results
// That helps in analyzing data

// SubSection using the aggregate method
// the $match is used to pass the documetns taht meet the
// specified conditions to the next pipeline stage/ aggregation process.
// syntax
//  {$match: {$field: value}}
//  {$match: {$group: _id: value, fieldResult: "valueResult"}}
// using match and group along with aggregation will find for products that are on sale and will group the total amount of stocks for suppliers found
// syntax:
// db.collectionName.aggregate([
// {$match: {fieldA, valueA}},
// {$group: {$_id: "$fieldB"},{result: {operation}}}
// ])

// the symbol "$" will refer t oa field name that is available in the documents
// that are being aggregated on.

db.fruits.aggregate([
		{$match: { onSale: true } }, //like if condition
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}} //grouped by interms of supplier id.

	])
// sub section field projectio nwith aggregation
// the $project can be used when aggregating data to include/exclude fields
// froim the returned results

// syntax:
	// { $project : { field: 1/0}}

db.fruits.aggregate([
		{$match: { onSale: true } }, //like if condition
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{$project: {_id: 0}}

	])

// sort $sort 
// syntax
// {$sort: {_id: 1/-1}}
db.fruits.aggregate([
		{$match: { onSale: true } }, //like if condition
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{$sort: {total: -1}}

	])

// sub section aggregating resutls based on array fields
// the $unwind deconstructs an array fields from a collection/field with an array
// value to output a result for each elemnt

// syntax
//  {$unwind: "field"}

db.fruits.aggregate([
	{$unwind: "$origin"}
])

// subsection
// display fruits documents by their origin and the kind of fruits that are supplied

db.fruits.aggregate([
		{$unwind: "$origin"},
		{$group: {_id: "$origin", kinds: {$sum: 1}}}
	])